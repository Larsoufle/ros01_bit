#include <stdint.h>
#include <stddef.h>
#include <NoRTOS.h>
#include <ti/drivers/GPIO.h>
#include "Board.h"
#include "hulp.h"

int main(void)
{
    Board_initGeneral();
    NoRTOS_start();
    GPIO_init();

    while(1)
    {
       for (uint8_t i = 0; i < 8; i++)
       {
           GPIO_write(Board_GPIO_LED0, i&1);

           GPIO_write(Board_GPIO_LED1, (i >> 1)&1);

           GPIO_write(Board_GPIO_LED2, i&4 >> 2);

           delay(1000);
       }

        GPIO_toggle(Board_GPIO_LED0);
    }
}
