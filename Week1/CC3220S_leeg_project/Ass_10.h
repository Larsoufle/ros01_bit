#ifndef ASS_10H_
#define ASS_10_H_

/*
 * Omschrijving programma:
 * Dit programma laat de 3 onderste LEDs (D8, D9 en D10) knipperen
 * op zo'n manier dat er binair wordt opgeteld.
 *
 * Auteurs:
 *      Matthijs de Jonge    (0864820)   ER7IA
 *      Lars van der Kaaden  (0927400)   ER7IA
 */

#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
#include "hulp.h"

void ass10(void)
{
   HWREG(ARCM_BASE + APPS_RCM_O_GPIO_B_CLK_GATING) = 0x01;                  // aanzetten van de clock van GPIOA1
   HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0x20;          // koppelen van de LED pinnen aan de GPIO module
   HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0x20;
   HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0x20;
   HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0xE;                              // het instellen van de GPIO pinnen als outputs
   HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0x0E << 2)) = 0x00;              // het initialiseren van de LEDs op 0

    while(1)
    {
        for (uint8_t i = 0; i < 8; i++)
        {
            HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0x0E << 2)) = (i << 1); // het aansturing van de 3 LEDs
            delay(1000);
        }
    }
}
#endif
