#ifndef ASS_12_H_
#define ASS_12_H_

/*
 * Omschrijving programma:
 * Dit programma initialiseert GPIOA1 als output (zie register view)
 * Er wordt gebruik gemaakt van de driverlib ipv de HWREG functie.
 *
 * Auteurs:
 *      Matthijs de Jonge    (0864820)   ER7IA
 *      Lars van der Kaaden  (0927400)   ER7IA
 */

#include <gpio.h>
#include <pin.h>
#include <prcm.h>

void ass12(void)
{
    PRCMPeripheralClkEnable(PRCM_GPIOA1 ,PRCM_RUN_MODE_CLK);    // aanzetten van de clock van GPIOA1
    while(1);
}
#endif
