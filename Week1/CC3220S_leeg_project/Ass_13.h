#ifndef ASS_13_H_
#define ASS_13_H_

/*
 * Omschrijving programma:
 * Dit programma initialiseert GPIOA1 als output (zie register view)
 * Ook worden de 3 onderste LEDs (D8, D9 en D10) tegelijkertijd laten knipperen.
 * Er wordt gebruik gemaakt van de driverlib ipv de HWREG functie.
 *
 * Auteurs:
 *      Matthijs de Jonge    (0864820)   ER7IA
 *      Lars van der Kaaden  (0927400)   ER7IA
 */

#include <gpio.h>
#include <pin.h>
#include <prcm.h>
#include "hulp.h"

#include <stdint.h>
#include <stddef.h>
#include "register_def.h"
#include "hulp.h"

void ass13(void)
{
    // HWREG(ARCM_BASE + APPS_RCM_O_GPIO_B_CLK_GATING) = 0x01;                  // aanzetten van de clock van GPIOA1
    PRCMPeripheralClkEnable(PRCM_GPIOA1 ,PRCM_RUN_MODE_CLK);                    // aanzetten van de clock van GPIOA1

    // HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_9) = 0X20;          // koppelen van GPIO_09 aan de GPIO module
    PinTypeGPIO(PIN_64, PIN_STRENGTH_2MA, false);                               // koppelen van GPIO_09 aan de GPIO module
    PinTypeGPIO(PIN_01, PIN_STRENGTH_2MA, false);                               // koppelen van GPIO_10 aan de GPIO module
    PinTypeGPIO(PIN_02, PIN_STRENGTH_2MA, false);                               // koppelen van GPIO_11 aan de GPIO module

    //HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_10) = 0X20;
    //HWREG(OCP_SHARED_BASE + OCP_SHARED_O_GPIO_PAD_CONFIG_11) = 0X20;

    GPIODirModeSet(GPIOA1_BASE, 0x0E, GPIO_DIR_MODE_OUT);

    //HWREG(GPIOA1_BASE + GPIO_O_GPIO_DIR) = 0xE;                               // het instellen van de GPIO pinnen als outputs
    //HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0x0E << 2)) = 0x00;                 // het initialiseren van de LEDs op 0

    GPIOPinWrite(GPIOA1_BASE, 0x0E, 0x00);

    while(1)
    {
        //HWREG(GPIOA1_BASE + GPIO_O_GPIO_DATA + (0x0E << 2)) ^= 0xE;             // het toggle-en van de 3 LEDs

        GPIOPinWrite(GPIOA1_BASE, 0x0E, 0x0E);

        delay(1000);

        GPIOPinWrite(GPIOA1_BASE, 0x0E, 0x00);

        delay(1000);
    }
}
#endif
