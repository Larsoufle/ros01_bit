#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ti/sysbios/BIOS.h>
#include "Board.h"

void check(int error)
{
    if (error != 0)
    {
        printf("Error: %s\n", strerror(error));
        while (1);
    }
}

int aantal = 0;
pthread_mutex_t m;

void *teller(void *par)
{
    for (int i = 0; i < 10000; i++)
    {
        check( pthread_mutex_lock(&m) );
        aantal++;
        check( pthread_mutex_unlock(&m) );
    }
    return NULL;
}

void *main_thread(void *arg)
{
    pthread_mutexattr_t ma;
    check( pthread_mutexattr_init(&ma) );
    check( pthread_mutex_init(&m, &ma) );

    pthread_attr_t pta;
    check( pthread_attr_init(&pta) );
    check( pthread_attr_setstacksize(&pta, 1024) );

    pthread_t t1, t2, t3;
    check( pthread_create(&t1, &pta, &teller, NULL) );
    check( pthread_create(&t2, &pta, &teller, NULL) );
    check( pthread_create(&t3, &pta, &teller, NULL) );

    check( pthread_join(t1, NULL) );
    check( pthread_join(t2, NULL) );
    check( pthread_join(t3, NULL) );

    printf("aantal = %d\n", aantal);

    return NULL;
}

int main(void)
{
    pthread_attr_t pta;
    check( pthread_attr_init(&pta) );
    check( pthread_attr_setdetachstate(&pta, PTHREAD_CREATE_DETACHED) );
    check( pthread_attr_setstacksize(&pta, 2048) );

    struct sched_param sp;
    check( pthread_attr_getschedparam(&pta, &sp) );
    // The main thread must have the highest priority because this thread will start
    // the other threads and we want to study the interaction between those other threads
    sp.sched_priority = 15;
    check( pthread_attr_setschedparam(&pta, &sp) );

    pthread_t pt;
    check( pthread_create(&pt, &pta, main_thread, NULL) );

    printf("\n");
    BIOS_start();
    return EXIT_SUCCESS;
}
