#include <stdint.h>
#include <stddef.h>
#include <NoRTOS.h>
#include <ti/drivers/GPIO.h>
#include "Board.h"

int main(void)
{
    Board_initGeneral();
    NoRTOS_start();
    GPIO_init();

    while(1)
    {
        for (volatile int teller = 0; teller < 5714286; teller++);
        GPIO_toggle(Board_GPIO_LED0);
    }
}
